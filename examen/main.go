package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
)

//Partie_1
type Task struct {
	Description string
	Done        bool
}

var tasks []Task

func main() {

	http.HandleFunc("/", list)
	http.HandleFunc("/done", done)
	http.HandleFunc("/add", add)

	log.Fatal(http.ListenAndServe(":8080", nil))
}

//Partie_2

func list(rw http.ResponseWriter, _ *http.Request) {
	list := []List{}
	tasks = []Task{
		{"Faire les courses", false},
		{"Payer les factures", true},
		{"Récupérer les enfants", false},
	}
	for id, el := range tasks {
		if !el.Done {
			list = append(list, List{strconv.Itoa(id), el.Description})
		}
	}
	rw.WriteHeader(http.StatusOK)
	data, _ := json.Marshal(list)

	rw.Write(data)
	return
}

type List struct {
	ID   string `json:"id"`
	Task string `json:"task"`
}

func done(w http.ResponseWriter, r *http.Request) {
	fmt.Println("done")
}
func add(w http.ResponseWriter, r *http.Request) {
	fmt.Println("add")
}
